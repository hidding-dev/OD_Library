﻿**20 E Olive Street STE 208** • **PO Box 1230 Bozeman, ![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.001.png)MT 59771-1230**

**Phone: (406) 582-2375 [ permits@bozeman.net**](mailto:permits@bozeman.net)**

**Residential Accessible, Type A, and Type B Dwelling Unit Requirements 2012 International Building Code and 2009 ICC/ANSI A117.1** 

**Group R-1**  

Accessible Units: In group R-1 occupancies, Accessible dwelling and sleeping units shall be provided in accordance with Table 1107.6.1.1. All facilities on site shall be considered to determine the total number of Accessible units. Accessible units shall be dispersed among the various classes of units. Roll in showers provided in Accessible units shall include a permanently mounted folding shower seat. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.002.png)

Type B Units 

In structures with four or more dwelling or sleeping units intended to be occupied as a residence** in a single structure, every dwelling unit and sleeping unit intended to be occupied as a residence shall be a Type B Unit. 

**Group R-2** 

Type A Units: Type A units in Group R-2 occupancies containing more than 20 dwelling or sleeping units, at least 2 percent but not less than one of the units shall be a Type A unit. All units on a site shall be considered to determine the total number of units and the required number of Type A units. 

Type B Units: Where there are four or more dwelling or sleeping units intended to be occupied as a residence** in a single structure, every dwelling unit and sleeping unit intended to be occupied as a residence shall be a Type B Unit. 

**Group R-2 other than apartment houses, monasteries, and convents.** 

Accessible Units: Accessible units shall be provided in accordance with Table 1107.6.1.1. 

Type B Units: Where there are four or more dwelling or sleeping units intended to be occupied as a residence** in a single structure, every dwelling unit and sleeping unit intended to be occupied as a residence shall be a Type B Unit. 

**Group R-3**  

In Group R-3 occupancies where there are four or more dwelling or sleeping units intended to be occupied as a residence** in a single structure, every dwelling unit and sleeping unit intended to be occupied as a residence shall be a Type B Unit. 

**Group R-4**  

Accessible Units: At least one of the dwelling or sleeping units shall be an Accessible unit. 

Type B Units: In structures with four or more dwelling or sleeping units intended to be occupied as a residence** in a single structure, every dwelling unit and sleeping unit intended to be occupied as a residence shall be a Type B Unit. 

**One story with Type B units required:** At least one story containing dwelling units or sleeping units intended to be occupied as a residence shall be provided with an accessible entrance from the exterior of the structure and all units intended to be occupied as a residence on that story shall be Type B units. 

**Multistory Units:** A multistory dwelling or sleeping unit which is not provided with elevator service is not required to be a Type B unit. 

Where elevator service in the building provides an accessible route to only the lowest story containing dwelling or sleeping units, only the units on that story are required to be Type B units. 

**Definitions:** 

**Accessible:** For multi-family dwellings, accessible means the public or common-use areas of the building that can be approached, entered, and used by individuals with physical disabilities. The phrase, “readily accessible to and usable by” is synonymous with accessible. 

**Accessible Route:** A continuous unobstructed path connecting all accessible elements and spaces in an accessible building or facility that can be negotiated by a person with a severe disability using a wheelchair and that is also safe and usable by persons with other disabilities. 

**Accessibility:** The combination of various elements in a building or area which allows access, circulation, and the full use of the building and facilities by persons with physical disabilities. 

**Adaptable Dwelling Unit:** An accessible dwelling unit within a multi-family structure as designed with elements and spaces allowing the dwelling units to be adapted or adjusted to accommodate the user. 

**Equivalent Facilitation:** An alternate means of complying with the literal requirements of the standards and specifications in the code that provide access in terms of the purpose of those standards and specifications. 

*Note: In determining equivalent facilitation, consideration shall be given to means that provide for the maximum independence of persons with disabilities while presenting the least risk of harm, injury or other hazard to such persons, or others.* 

**Facility (or Facilities):** A building, structure, room, site, complex, or any portion thereof, that is built altered, improved, or developed to serve a particular purpose. 

**Slope:** For the purpose of multi-family buildings, the relative steepness of the land between two points is calculated as follows: *The horizontal distance and elevation change between the two points (e.g. an entrance and a passenger loading zone).*  

**(Slope)** *The difference in elevation is divided by the distance between points, and the resulting fraction is multiplied by 100 to obtain the percentage of slope. For example: If a principal entrance is 10 feet from a passenger loading zone, and the principal entrance is raised 1 foot higher than the passenger loading zone, then the slope is 1/10 X 100 = 10 percent.* 

**Accessible Dwelling Units:** *Accessible units are considered to provide a higher level of accessibility than both Type A and Type B units. Accessible dwelling or sleeping units are constructed to be wheelchair accessible. See the scoping requirements in Chapter 11 of the IBC for when accessible dwelling and sleeping units are required. Accessible units are typically required in transient facilities such as hotels or facilities where there is a high anticipation of people who may need these facilities, such as nursing homes.*** 

` `**Primary Entrance**  

The accessible primary entrance shall be on an accessible route from public and common areas.* The accessible entrance cannot be a “back door” entrance such as a patio door or through a bedroom. This accessible unit entrance must be connected by an accessible route to an accessible building entrance (if applicable) and all public or shared areas intended for the use of the residents. This includes areas such as a building lobby, mail boxes, garbage chutes or dumpsters, shared laundry facilities, and recreational facilities such as exercise rooms or pools. 

**Accessible Route** 

Accessible routes within Accessible units shall comply with ICC/ANSI A117.1 Section 1002.3.  

- At least one accessible route shall connect all spaces and elements that are a part of the unit. Where only one accessible route is provided, it shall not pass through bathrooms and toilet rooms, closets, or similar spaces. 

**Exception:** *An accessible route is not required to unfinished attics and unfinished basements that are part of the unit.* 

- All rooms served by an accessible route shall provide a turning space complying with ICC/ANSI A117.1 Section 304. 
- Accessible routes shall consist of one or more of the following elements: walking surfaces with a slope not steeper then 1:20, ramps, elevators, and platform lifts. 
- Walking surfaces that are part of an accessible route must be stable, usable, and not contain obstructions that would prevent their use in reaching an accessible element. Walking surfaces shall comply with ICC/ANSI A117.1 Section 403.** 

**Turning Space** 

Turning spaces are required in each room. This space can be circular or T-shaped. The turning space can include knee and toe clearances under fixtures, counters, shelves, etc. 

**Doors and Doorways** 

The provisions for doors and doorways are applicable only to doors that are part of the accessible route. Doors that are not part of the accessible route need not comply with ICC/ANSI A117.1 Section 404.  

- The primary entrance door to the unit, and all other doorways intended for user passage, shall comply with ICC/ANSI A117.1 Section 404. 
- Doorways shall have a clear opening width of 32 inches measured between the face of the door and stop, with the door open 90 degrees. 
- Openings, doors, and doorways more than 24 inches in depth shall provide a clear opening of 36 inches minimum. 
- There shall be no projections into the clear opening width lower than 34 inches above the floor. 
- Projections into the clear opening width between 34 and 80 inches above the floor shall not exceed 4 inches. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.003.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.004.png)

**Door Threshold** 

**Two Doors in a Series** 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.005.png)

**Door Hardware** 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.006.png)

- Ramps shall comply with ICC/ANSI A117.1 Section 405. 
- Elevators within the unit shall comply with ICC/ANSI A117.1 Sections 407, 408, or 409. 
- Platform lifts within the unit shall comply with ICC/ANSI A117.1 Section 410. 

**Operable Parts** 

Lighting controls, electrical switches and receptacle outlets, environmental controls, appliance controls, operating hardware for operable windows, plumbing fixture controls, and user controls for security or intercom systems shall comply with ICC/ANSI A117.1 Section 309 and Section 1002.9. 

**Laundry Equipment** 

Laundry equipment is not required within a unit, however if provided, the washers and dryers must meet the same accessibility provisions as in a common laundry room. See ICC/ANSI A117.1 Section 611. Stacked washers and dryers typically do not meet the same accessibility provisions as in a common laundry room or the reach range for operable parts. 

Adequate turning radius or space for parallel approach is required. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.007.png)![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.008.png)

**Toilet and Bathing Facilities** 

Every bathroom in Accessible units must be constructed as fully accessible in accordance with the requirements of ICC/ANSI A117.1 Sections 603 through 610. This includes grab bars installed at the water closet and tub or shower, a water closet that allows for a side transfer, a front approach lavatory, and an accessible bathing fixture (tub or shower).  

*Unlike Type A and Type B unit bathrooms, the lavatory may not overlap the clear floor space for the water closet.* 

If vanity counter top space is provided in non-accessible dwelling or sleeping units with the same facility, equivalent vanity counter top space, in terms of size and proximity to the lavatory, shall also be provided in Accessible units. 

**Kitchens** 

Kitchens within an accessible unit must be as constructed accessible in accordance with ICC/ANSI A117.1 Section 804. This would include a front approach sink and an accessible work surface along with access to each appliance. A turning space is also required within the room. If the space under the sink or the work surface is used as part of a T-turn, the width must be a minimum of 36 inches. 

**Windows** 

Where operable windows are provided, at least one window in each sleeping, living, or dining space shall have operable parts that meet the requirements of ICC/ANSI A117.1 Section 1002.9. Operating hardware for operable windows includes locks and opening devices for windows that must be accessible. At this time, to meet the 5-pound maximum operating force requirement, most windows need add-on devices for opening. 

**Storage Facilities** 

Storage facilities such as closets, bathroom cabinets or pantries, must have a clear floor space in front of the storage element. At least a portion of each storage facility provided, such as shelves or rods must be within the 15 to 48-inch reach range. A standard closet organizer with a high-low rod for a portion of the closet meets this provision. If there are doors and drawers, the latches and knobs must be easily operable by a person with limited hand movement and strength. If this is a reach-in closet, the door does not have to meet the clear width, threshold, or maneuvering clearance requirements. Kitchens should comply with ICC/ANSI A117.1 Section 804.5. 

**Type A Units** 

Type A units are considered to provide a higher level of accessibility than Type B units, but less accessibility than Accessible units. Type A units have some elements constructed as wheelchair accessible such as clear doors with maneuvering clearances, controls with reach ranges, and some element that allow for planning for those elements to made accessible such as sink and work space in the kitchen and the bathroom lavatory.  

When fully adapted, the Type A units come close to meeting the level of access found in accessible units. The bathrooms, depending on the configuration, may have a lesser degree of accessibility. See the scoping requirements in Chapter 11 of the IBC for when Type A dwelling and sleeping units are required. Type A Units are typically found in large apartment and condo buildings. 

**Primary Entrance** 

The accessible primary entrance shall be on an accessible route from public and common areas. The accessible entrance cannot be a “back door” entrance such as a patio door or through a bedroom. This accessible unit entrance must be connected by an accessible route to an accessible building entrance (if applicable) and all public or shared areas intended for the use of the residents. This includes areas such as a building lobby, mail boxes, garbage chutes or dumpsters, shared laundry facilities, and recreational facilities such as exercise rooms or pools. 

**Accessible Route** 

Accessible routes within Accessible units shall comply with ICC/ANSI A117.1 Section 1003.3.  

- At least one accessible route shall connect all spaces and elements that are a part of the unit. Where only one accessible route is provided, it shall not pass through bathrooms and toilet rooms, closets, or similar spaces and should not run through areas that are subject to locking. 

**Exception:** *An accessible route is not required to unfinished attics and unfinished basements that are part of the unit.* 

- All rooms served by an accessible route shall provide a turning space complying with ICC/ANSI A117.1 Section 304. 
- Accessible routes shall consist of one or more of the following elements: walking surfaces with a slope not steeper then 1:20, ramps, elevators, and platform lifts. 
- Walking surfaces that are part of an accessible route must be stable, usable, and not contain obstructions that would prevent their use in reaching an accessible element. Walking surfaces shall comply with ICC/ANSI A117.1 Section 403. 

**Turning Space** 

Turning spaces are required in each room. This space can be circular or T-shaped. The turning space can include knee and toe clearances under fixtures, counters, shelves, etc 

In Type A units with multiple bathrooms, only one bathroom must meet the clearances in ICC/ANSI A117.1 Section 1003.11. The bathroom that is not accessible does not have to have a turning space within the rooms. The bathroom that serves as the accessible bathroom is allowed to have the turning space available with the knee and toe clearances planned for under the lavatory (after the cabinets have been removed) rather than at the time of initial construction. 

**Doors and Doorways** 

The provisions for doors and doorways are applicable only to doors that are part of the accessible route. Doors that are not part of the accessible route need not comply with ICC/ANSI A117.1 Section 404.  

- The primary entrance door to the unit, and all other doorways intended for user passage, shall comply with ICC/ANSI A117.1 Section 404. 
- Doorways shall have a clear opening width of 32 inches measured between the face of the door and stop, with the door open 90 degrees. 
- Openings, doors, and doorways more than 24 inches in depth shall provide a clear opening of 36 inches minimum. 
- There shall be no projections into the clear opening width lower than 34 inches above the floor. 
- Projections into the clear opening width between 34 and 80 inches above the floor shall not exceed 4 inches. 
- Only one bathroom in a multi bathroom Type A unit need be accessible. Maneuvering is not required on the inside of the door, but is required on the outside. 
- The doors to the accessible bathroom shall not swing into the clear floor space or clearance of any fixture. There is an exception to this rule that allows where a 30 inch by 48-inch wheelchair space is provided past the swing of the door, the door can swing over the fixture clearances. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.009.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.010.png)

**Door Threshold** 

**Ramps** shall comply with ICC/ANSI A117.1 Section 405. 

**Elevators** within the unit shall comply with ICC/ANSI A117.1 Sections 407, 408, or 409. **Platform Lifts** within the unit shall comply with ICC/ANSI A117.1 Section 410. 

**Operable Parts** 

Lighting controls, electrical switches and receptacle outlets, environmental controls, appliance controls, operating hardware for operable windows, plumbing fixture controls, and user controls for security or intercom systems shall comply with ICC/ANSI A117.1 Section 309 and Section 1003.9. 

**Laundry Equipment** 

Laundry equipment is not required within a unit, however if provided, the washers and dryers must meet the same accessibility provisions as in a common laundry room. See ICC/ANSI A117.1 Section 611. Stacked washers and dryers typically do not meet the same accessibility provisions as in a common laundry room or the reach range for operable parts. 

Adequate turning radius or space for parallel approach is required. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.007.png)![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.008.png)

**Toilet and Bathing Facilities** 

Unlike an Accessible unit, when a Type A unit has multiple bathrooms, only one bathroom in the unit must be accessible. In addition, there are provisions that allow for planning for the future installation of the grab bars and installing cabinetry under the lavatory. There are also allowances for lavatories to overlap the clearance of the water closet in Type A units that are not permissible on Accessible units. 

The inaccessible bathroom must have blocking in the walls for the future installation of grab bars, but the fixture clearances, maneuvering clearances, and other items required for an Accessible or Adaptable bathroom are not required. 

In Type A bathrooms, either the grab bars must be installed or blocking must be provided for future installation of grab bars at the water closet and bathtub or shower. ICC/ANSI A117.1 Sections 604.5, 607.4, 608.3, and 608.4 specifies the length and location of grab bars. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.011.png)

**Lavatory** 

The lavatory is one of the elements that are permitted to be adaptable in Type A units. The base requirement is for a lavatory with a maximum height of 34 inches, a clear floor space for a front approach with associated knee and toe clearances underneath the lavatory, and faucets that meet the operable parts requirements. Pipes underneath must be padded or configured to prevent accidental contact that may result in an injury. This is the same as required in an Accessible unit. 

Exception: Cabinetry shall be permitted under the lavatory, provided: 

1. The cabinetry can be removed without removal or replacement of the lavatory; 
1. The floor finish extends under such cabinetry; and 
1. The walls behind and surrounding cabinetry are finished. 

Mirrors above lavatories shall have the bottom edge of the reflecting surface 40 inches’ maximum above the floor. 

**Water Closet** 

The water closet shall be positioned with a wall to the rear and to one side. The centerline of the water closet shall be 16 inches minimum and 18 inches’ maximum from the side wall. Note: These are the same dimensions required for an Accessible unit water closet. 

A clearance around the water closet of 60 inches minimum, measured perpendicular from the side wall, and 56 inches measured perpendicular from the rear wall shall be provided. 

The required clearance around the water closed shall be permitted to overlap the water closet, grab bars, paper dispensers, coat hooks, shelves, accessible routes, clear floor spaces required for other fixtures, and the wheelchair turning space. No other fixtures or obstructions shall be located within the required water closet clearance. 

Exception: A lavatory complying with ICC/ANSI A117.1 Section 1003.11.5 shall be permitted on the rear wall 18 inches minimum from the centerline of the water closet where the clearance at the water closet is 66 inches’ minimum measured perpendicular from the rear wall. 

The top of the water closet seat shall be 15 inches minimum and 19 inches’ maximum above the floor, measured to the top of the seat. 

**Water Closet Location** 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.012.png)

**Bathtub** 

If the accessible bathing fixture is a bathtub, the bathtub must comply with ICC/ANSI A117.1 Section 607. A clear floor space of 30 inches by the length of the tub is required. No fixtures can overlap the clear floor space in front of the tub. There is an exception that allows for a portion of a vanity to overlap the clear floor space of the tub if this portion can be easily removed should the occupant need the space to access the tub. 

**Shower** 

If the accessible bathing fixture is a shower, it can be a transfer shower, roll-in shower, or alternative roll-in shower as specified in ICC/ANSI A117.1 Section 608. There is an exception that allows for a portion of a vanity to overlap the clear floor space of the tub if this portion can be easily removed should the occupant need the space to access the shower. 

**Clearance for Bathtubs in Type A Units** 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.013.png)

**Standard Roll-in Type Shower Compartment in Type A Units** 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.014.png)

**Kitchens** 

Type A Unit Kitchens include some parts constructed accessible and some elements constricted with accessibility planned for as an adaptable element.  

A turning space is required within the kitchen. If the space under the sink or work surface (once they are adapted) is to be used as part of a T-turn, the width must be a minimum of 36 inches.  

A Galley type kitchen is typically laid out with appliances, counters, and sinks in straight lines and is open on one or both ends. In a Galley type kitchen, the minimum clear width between opposing cabinets/appliances or cabinet/appliances and a wall or other type of obstruction is 40 inches minimum. This measurement does not include cabinet or appliance pulls or handles. 

U-shaped Kitchens are kitchens with cabinets and/or appliances on 3 contiguous sides. In a U-Shaped kitchen the minimum clear width between opposing cabinets/appliances or cabinet/appliances and a wall or other type of obstruction is 40 inches minimum. This measurement does not include cabinet or appliance pulls or handles. 

At least one section of counter shall provide a work surface a minimum of 30 inches in length and 34 inches’ maximum above the finish floor surface. If the work surface (once it is adapted) is to be used as part of a T-turn, the width must be a minimum of 36 inches.  

In addition to installing counter or work surfaces at an accessible height, a work surface must be on an accessible route and have adequate clearances under that surface. A front approach is required at work surfaces. 

Sinks in kitchens must have knee and toe clearances that allow for a front approach (including projections from pipes), a maximum rim height of 34 inches, and faucets that meet operable parts requirements. If a double bowl sink is installed, these requirements can apply to only one bowl. If the space under the sink (once it is adapted) is to be used as part of a T-turn, the width must be a minimum of 36 inches.  

Water supply and drainage piping under sinks shall be insulated or otherwise configured to protect against contact. There shall be no sharp or abrasive surfaces under sinks. 

Galley Style Kitchens 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.015.png)

U-Shaped Kitchen 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.016.png)

Work Space Area 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.017.png)

Kitchen Sink 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.018.png)

Kitchen Sink 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.019.png)

Ovens shall comply with ICC/ANSI A117.1 Section 1003.12.6.5.  

Side hinged oven doors shall have a countertop positioned adjacent to the latch side of the oven door. Bottom hinged oven doors shall have a countertop positioned on either side of the oven door. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.020.png)

Refrigerator/Freezers shall comply with ICC/ANSI A117.1 Section 1003.12.6.6. 

Refrigerator/ Freezer choices can have the freezer on the top, on the bottom, or side by side. If a top freezer option is chosen, a freezer with the bottom of the compartment at a maximum of 54 inches above the floor (assuming one shelf in the freezer) will meet the freezer compartment requirements. 

If a bottom freezer option is chosen, at least one shelf in the freezer compartment must be at least 15 inches minimum above the floor. Bottom freezers are typically pull out drawers, so a clear floor space must be available with the freezer door fully open. 

If the refrigerator has an ice maker or water available through the door or inside the appliance, those elements must also have a clear floor space for access and meet the reach range, and operable parts requirements. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.021.png)A trash compactor is similar to the dishwasher and oven in the fact that it must be opened for use. Therefore, the clear floor space should be located so that it does not obstruct the door of the trash compactor in the open position. 

Trash Compactor and Dishwasher Clear Floor Space                           

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.022.png)

**Storage Facilities** 

The provisions for storage in Type A units shall comply with ICC/ANSI A117.1 Section 1003.12.5. Kitchen Cabinets shall comply with ICC/ANSI A117.1 Section 1003.12.5. 

A 30 inch by 48-inch clear floor space must be provided at each storage element. Centering is not a requirement; rather, the access into the element should be considered. If access to the storage element is confined in any way on 3 sides, additional maneuvering space will be required. 

At least a portion of each storage facility, such as shelves or rods must be within the 15 to 48 reach range. 

Door hardware or cabinet or drawer latches or pulls must be within reach ranges and be easy to operate. 

**Type B Units** 

Type B units are intended to be consistent with the Fair Housing Accessibility Guidelines. See the International Building Code scoping documents for when Type B dwellings and sleeping units are required. 

Type B units are typically constructed together. This can include both institutional and residential type facilities. 

Note that Type B units do not include requirements for storage and windows such as those found in Accessible and Type A units. 

**Primary Entrance** 

The accessible primary entrance shall be on an accessible route from public and common areas. The accessible entrance cannot be a “back door” entrance such as a patio door or through a bedroom. This accessible unit entrance must be connected by an accessible route to an accessible building entrance (if applicable) and all public or shared areas intended for the use of the residents. This includes areas such as a building lobby, mail boxes, garbage chutes or dumpsters, shared laundry facilities, and recreational facilities such as exercise rooms or pools. 

**Accessible Route** 

*Type B units differ from Accessible and Type A units in that the accessible route does not require a turning space anywhere within the unit. The accessible route in the Type B units must meet the provisions for location and components only.* 

Accessible routes within Accessible units shall comply with ICC/ANSI A117.1 Section 1004.3.  

- At least one accessible route shall connect all spaces and elements that are a part of the unit. Where only one accessible route is provided, it shall not pass through bathrooms and toilet rooms, closets, or similar spaces and should not run through areas that are subject to locking. 

**Exception:** *An accessible route is not required to unfinished attics and unfinished basements that are part of the unit.* 

- Accessible routes shall consist of one or more of the following elements: walking surfaces with a slope not steeper then 1:20, ramps, elevators, and platform lifts. 
- Walking surfaces that are part of an accessible route must be stable, usable, and not contain obstructions that would prevent their use in reaching an accessible element. Walking surfaces shall comply with ICC/ANSI A117.1 Section 403. 

**Doors and Doorways** 

The provisions for doors and doorways are applicable only to doors that are part of the accessible route. Doors that are not part of the accessible route need not comply with ICC/ANSI A117.1 Section 404.  

- The primary entrance door to the unit, and all other doorways intended for user passage, shall comply with ICC/ANSI A117.1 Section 404. 

*Exception: Maneuvering clearances required by Section 404.2.3 shall not be required on the unit side of the primary entrance door, but maneuvering space is required on the outside.* 

- The primary entrance door to a Type B unit must meet all the provisions for doors that are located along an accessible route. This included requirements for clear width, thresholds, lever hardware, door opening force, bottom door surface on the push side, and vision lite location. Note: a front door with a screen/storm door is not considered as doors in series. 
- Doorways shall have a clear opening width of 32 inches measured between the face of the door and stop, with the door open 90 degrees. 
- There shall be no projections into the clear opening width lower than 34 inches above the floor. 
- Projections into the clear opening width between 34 and 80 inches above the floor shall not exceed 4 inches. 
- Thresholds shall comply with ICC/ANSI A117.1 Section 303 and 1004.5.2.2. 

**Ramps** shall comply with ICC/ANSI A117.1 Section 405. 

**Elevators** within the unit shall comply with ICC/ANSI A117.1 Sections 407, 408, or 409. **Platform Lifts** within the unit shall comply with ICC/ANSI A117.1 Section 410. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.023.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.024.png)

**Door Threshold** 

**Operable Parts** 

Lighting controls, electrical switches and receptacle outlets, environmental controls, appliance controls, operating hardware for operable windows, plumbing fixture controls, and user controls for security or intercom systems shall comply with ICC/ANSI A117.1 Section 309 and Section 1004.9. 

In a Type B unit, the counters can be located at any height, typically 36 inches. However, with the obstructed side reach range requirements in ICC/ANSI A117.1 Section 308.3.2, receptacles cannot be located over the standard 36-inch-high counter and be accessible. An alternative is to locate a receptacle on the side or front surface of the lower cabinet. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.025.png)

In Type B units, plumbing fixture controls are exempted per Exception 7. Plumbing control fixtures would include faucets in showers, tubs and sinks, and flush valves on toilets which do have accessibility requirements in Accessible and Type A units. 

Accessible windows and window hardware are required for Accessible and Type A units, but they are not required in Type B units. Operational parts on the primary entrance door are required to be accessible, but door hardware on other doors within the unit are not regulated. See ICC/ANSI A117.1 Section 1004.5.2. 

**Laundry Equipment within Type B Dwelling Units** 

Washing machines and clothes dryers shall comply with ICC/ANSI A117.1 Section 1004.10. A clear floor space complying with ICC/ANSI A117.1 Section 305.3, positioned for parallel approach shall be provided. The clear floor space shall be centered on the appliance. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.026.png)

**Toilet and Bathing Facilities** 

Toilet and bathing fixtures shall comply with ICC/ANSI A117.1 Section 1004.11.  

Exception: Fixtures that are not required to be accessible. 

Type B units allow for a minimal access into a bathroom. In Type B units, designers can choose to meet Option A or Option B requirements for the bathroom in the dwelling unit or serving the sleeping unit*. Note: Option A does not apply to Type A units.*  The choice for Option A (ICC/ANSI A117.1 Section 1004.11.3.1) or Option B (ICC/ANSI A117.1 Section 1004.11.3.2) is available for dwelling or sleeping units with only one bathroom. When there is more than one bathroom in the unit, Option A clearances would apply to all the bathrooms, and Option B clearances would apply to just one bathroom in the unit. 

All bathrooms must provide blocking for the future installation of grab bars.  

Whichever bathroom option is chosen, the clearances in those bathrooms must also meet ICC/ANSI A117.1 Section 1004.11.1. Once an option is chosen, the requirements are exclusive with no mixing of Option A and Option B requirements. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.027.png)

**Lavatory** 

The clear floor space requirement for Option A and Option B lavatories is the same, but Option B has an additional height requirement which allow a maximum of 34 inches in height. 

Cabinetry shall be permitted under the lavatory provided such cabinetry can be removed without removal or replacement of the lavatory, and the floor finish extends under such cabinetry.  

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.028.png)

**Water Closet** 

The water closet requirements for Option A and Option B bathrooms are the same except, Option A bathrooms allow a configuration with the water closet located between the tub and lavatory, and option B bathrooms require the water closet to be located next to a wall.  

The water closet must have a clearance of at least 15 inches on the side of the approach and 18 inches on the side away from the direction of approach. Accessible and Type A units allow for 16 inches to 18 inches from the centerline of the water closet to the wall. Type B units require 18 inches minimum. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.029.png)

**Bathing Facilities** 

Bathing facilities can be either a bathtub or a shower. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.030.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.031.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.032.png)

**Shower Compartment** 

If a shower compartment is the only bathing facility, the shower compartment shall have minimum dimensions of 36 inches in width by 36 inches in depth.  

A clearance of 48 inches minimum in length and 30 inches in depth shall be provided. Reinforcing for a shower seat is not required in shower compartments larger than 36 inches in width by 36 inches in depth. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.033.png)

**Kitchens:** 

The intent of the Type B unit kitchen is that the kitchen is usable by a person using a wheelchair. Consideration must be given for wheelchair access to the general kitchen area, sink, and appliances. 

The Type B unit kitchen does not include some of the access requirements found in Type A units, such as adaptability for front approach at the sink or a work surface, a turning space within the room, or specific requirements for access to the cabinets. 

A side approach to the kitchen sink is permitted. Type B and Type A units are similar in their requirements for clearances in the kitchen and approach to the appliances. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.034.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.035.png)

The clear floor space for the sink, dishwasher, cooktop, oven, built in microwave, refrigerator/freezer, trash compactor, etc. must be 30 X 48 inches. 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.036.png)

Clear floor space at the dishwasher: 

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.037.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.038.png)

![](Aspose.Words.b821057f-e9ab-43e5-b584-a4bdffee4f9c.039.png)
