- Title search
	- any encumbrances on the title?
- SF over overall building
  - SF of scope
- Neighboring tenants
- Sprinklers
- Adequate Exiting
- ADA compliance
  - main entry
  - restrooms
- Salvage
  - Acoustical ceilings
  - Doors
  - Lighting
  - Finishes
- Structure
	- Change of occupancy? Change of loads?  Existing structure to accommodate?
	- Determine structure to determine construction type
- Fire separation
	- Check doors
	- Stairs
- HVAC
  - locations of main trunks and equipment
  - suggested locations of proposed equipment
- Electrical
  - Lighting
- Plumbing
  - existing restrooms enough for new use?
  - sewer stub location
  - water stub location
  - location of hot water heater
  - existing drinking fountain
- Sprinklers
- Sitting
  - Parking
  - Signage
    - zoning implications
- Finishes - general strategy
- Allocated budget
- Schedule
- Drawings
	- Permit set
	- Reflected ceiling
	- Finish plan
	- MEP?
- Existing CAD files
- What are the neighboring tenants
	- possibility measure or do reconnaissance on their suite to find any conflicts with what's planned 



### Restaurants



- Main spaces
  - Entry
  - Kitchen
    - Prep
      - dedicated food prep sink
      - handsink
    - Cooking
      - Cooking: Grease
        - fryers
        - griddle
        - Type I hoods
        - Grease traps
      - Cooking: nonGrease
        - ovens
        - toaster
        - Type II hood
      - handsink
      - hoods
 
  - Dishwashing
    - Dish washing
      - dishwasher or 3 compartment sink
    - Type II Hood if heating
      - no hood, for smaller chemical DW
    - hand sink
  - storage
    - dry
    - refrigeration/freezer
    - mop sink, required
    - garbage strategy
  - Seating
    - seating types
  - Register
    - check out
    - displays
    - sneeze guards
    - refrigerated displays
    - Signage
    - Condiment area
  - Bathrooms
    - dishwasher or 3 compartment sink
    - taps
      - refrigeration lines
  - Restrooms
    - finishes
    - baby changing
    - Employee use
      - If public, faucet non-hand operated
  - offices
  - Equipment specs



<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIxMjE5MjczMjksLTEwNTA0NTU0MTMsLT
I3Mzc2MTY3LDY1MzAyMDgyNCwtNDgxMzI5ODQxLDkwMzA5Njc1
OSwxMzEzMTYxNzAwLDE4NTY5OTQyNTAsNjQwNTExMDU3LC0xNj
U5MDA0NTk4LC03NzYwMTkyNjUsMTIxMjQxMjI0MCwxNTA5MDg2
MDk0LC0xMTA2MTg5MTkyLDE4NTcyMDA0NzYsLTY0NTgzMzcyMC
wtMzM2NzEwNTk4LC01MjQ5NzY5LDIxMjc1NTQ0MjcsMTU3Nzk5
MTU1XX0=
-->