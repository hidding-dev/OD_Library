### Setting Up the Chat App

- We use [Element.io](https://element.io/) as our main chat app.  Each OpeningDesign project has it's own dedicated 'room'. 
  
  - If you go [here](https://gitlab.com/openingdesign) you can see a list of current and past projects and their associated chat rooms.
    - Majority of these rooms are public where past conversations can be reviewed by anyone, at anytime.
      - There are private channels, however, as well.
  - To install, go to https://element.io/get-started
    - Element has apps for Web, Android, iOS, macOS, Windows & Linux
    - Search and connect with ```@theoryshaw:matrix.org``` (Ryan Schultz)

### File Management on Windows

- If you use a file syncing software like Dropbox, and plan to sync OpeningDesign files to multiple machines (tower, laptop, etc.) make sure the path where the files are synced is the same. 
  
  - Examples:
    - Laptop: `D:\Dropbox\...`
    - Tower: `D:\Dropbox\...`

- download favorite text editor
  
  - example: [Atom.io](https://atom.io/)
    
    - Right click on the .exe file, and 'Run as administrator'
      
      - ![](imgs/admin_atom.png)

- Install **Git** for Windows
  
  - Download from [git-scm.com/download](https://git-scm.com/download)
  - 'Run as adminitrator' as you did with the text editor above.
  - Choose an appropriate installation location such as ```C:\Program Files\Git```
  - Select Components: Install the default components, including Git GUI Here and Git Bash Here
  - Choose default location of 'start menu folder'.  Will most likely default to the proper location.
  - Choose your preferred Git default editor.
    - For example, choose Atom.io, if that's your preferred text editor
  - Adjusting the name of the initial branch in new repositories.
    - Let Git decide
  - Choose recommended 'path environment'
  - use openSSL library
  - Accept the default line ending conversion for Unix and Windows compatibility
    - Checkout Windows-style, commit Unix-style line endings
  - Use MinTTY
  - Default (fast-forward or merge)
  - Git Credential Manager Core
  - Extra options
    - Enable file system caching
    - Enable GIT Credential Manager
  - Ignore experimental options
  - Click Finish to complete the install.

- A few Windows configurations...
  
  - Set longpath on windows
    
    - Long answer [here](https://www.youtube.com/watch?v=mAGQZ7RvKFk)
      - Short answer
        - Go to start menu and type 'regedit'
        - Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem
        - Right click on `LongPathsEnabled` and then `Modify...`
        - Value data: 1
  
  - Run **Git Bash** in administrator mode, like [this](https://www.dropbox.com/s/wk3l5weh1pt70oh/3TOLBa3Rs0.mp4?dl=0) and do the following 4 commands.  Do them in the exact following order.
    
    1. `git config --system core.longpaths true`
       - Long answer [here](https://stackoverflow.com/questions/22575662/filename-too-long-in-git-for-windows/22575737#22575737)
    2. `git config --system --unset core.autocrlf`
    3. `git config --global core.autocrlf true`
    4. `git config --global core.compression 0`
       - Long answer [here](https://stackoverflow.com/a/22317479)
    5. `git config --global http.postBuffer 1048576000`
	    - Long answer [here](https://stackoverflow.com/a/6849424/414643)

- Create accounts at...
  
  - Gitlab: go to https://gitlab.com/users/sign_up to create an account
    - Then set up Personal Access Token [here](https://gitlab.com/-/profile/personal_access_tokens)
      - ![](imgs/Gitlab_Personal_Access_Tokens_1.png)
      - Save the following token 'password' and don't share with anyone!  You will use this as a 'password' for pushing stuff to/from TortoiseGit
        ![](imgs/Gitlab_Personal_Access_Tokens_2.png)
  - Github: go to https://github.com/join to create an account
    - Then set up Personal Access Token [here](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)

- Download and install https://tortoisegit.org/download/
  
  - TortoiseGitPlink based on PuTTY...
  - Custom setup:
    - choose defaults
  - Run first start wizard
    - Language
    - Welcome screen
      - next
    - Point to git.exe (will probably default to the proper location)
    - Configure user information
      - add name and email *(preferably use the same email you used to with your Gitlab or Github accounts)*
    - Authentication and credential store
      - choose defaults

- Follow the steps at the following link to create your private/public GPG keys
  
  - [Windows - Creating New GPG Keys with Kleopatra](https://github.com/OpeningDesign/encryption-test/blob/master/README.md#windows---creating-new-gpg-keys-with-kleopatra)

<br>

---

### Cloning a Repo for the First Time

*Essentionally ''cloning' a repo means you're downloading the entire project folder onto your local machine.  Once it pulled down (downloaded) you don't have to clone it again.*

*After it's cloned, if there's a change on the repo you want to pull down to your local machine, follow the steps in the following section: [Pulling the latest changes down from the remote repo](#pulling-the-latest-changes-down-from-the-remote-repo)*.

1. Copy the **HTTPS** *(not SSH)* address from either Github or Gitlab.
   - From Gitlab...
     - ![](imgs/Gitlab_https.png)
   - From Github...
     - ![](imgs/Github_https.png)
2. Go to where you'd like to save the repo, and right click and 'Git Clone...' 
   - ![](imgs/clone_1.png)
   - The following fields should already be filed in and then hit 'okay'![](imgs/clone_2.png) 

<br>

---

<br>

### Pulling the latest changes down from the remote repo

1. Right click anywhere in the local folder and go to 'Git Sync'..
   - ![](imgs/sync_1.png)
   - And then 'Pull' ![](imgs/sync_2.png)

<br>

---

<br>

### Pushing a change

* right click anywhere in window to bring up the following context menu.  go to ***Git Commit -> "master"...***![](imgs/Git_commit.png)

* Fill in the following below, and hit **'Commit & Push'**![](imgs/commit_dialong.png)

* Make sure to hit **'Commit & Push'** instead of **'Commit'**.  Because **'Commit & Push'** syncs your change locally and on the remote, whereby **'Commit'** just commits your change locally.

<br>

---

<br>

### Starting a Project

- Create a [blank project](https://gitlab.com/projects/new#blank_project) on Gitlab
- [Clone the repo](#cloning-a-repo-for-the-first-time)
- Copy in all the template folders located here: ```OD_Library\Project Folder Structure``` and [push](#pushing-a-change).
- Run [git-crypt init](https://github.com/OpeningDesign/encryption-test#windows)
  - add users
- Tell collaborators to subscribe to Gitlab notifications.
  - [one way](imgs/gitlab_notifications.png)

<br>

---

<br>

### Revit Specific

- Try not to use the following, if possible...
  1. Dumb text notes. Prioritize the following instead.
     - 1st priority: Material Tags
     - 2nd priority: "OD_Keynote_Text Only" generic annotation
     - 3rd priority: dumb text
  2. Detail Lines and Detail Items.
     - Use 3D objects as much as possible
  3. override graphics view by element
     - use Visibility/Graphic Overrides for Categories instead, or better yet, use View Templates
  4. Paint a material on an object. (apply a material via a type, or to entire object)

<br>

---

<br>

### FreeCAD Specific

- Export IFC files very soon and very often, test them in [IFC++](https://ifcquery.com/) first, then in Revit. Detect issues early
- Use simple extrusions as much as possible (Arch Wall/Structures/Panels or Part Extrude)
- Objects exported as IFC structural elements (beams, columns) often give problems in Revit. If needed, use BuildingElement Proxies (Will come as generic models, Revit won't apply any transformation on them)
- Keep list of materials clean, merge duplicates, make sure all objects have a material

<br>
---
<br>

### Creating a New Revit Family

1. When creating a new revit family, one of the cardinal rules when making families is that the objects should be constained to reference planes.
2. There might be exceptions, but another cardinal rule is to start with a family template of the thing you're modeling... that is, use a window template for windows, door template for doors, etc.

<br>

---

<br>

### File too large to push to Github

If your file ever gets above 100mbs and you can't push to Github, try...

1. Purge all.  Run this (3) times to fully purge everything.
   ![](imgs/PurgeAll.png)
2. For whatever reason, if you do a 'save as' to a temporary file name, and do another 'save as' to overwrite the original file. it reduces the file size.

<br>

---

### Markdown Editor

- We use markdown a lot.  [Here's](https://yorik.uncreated.net/blog/2021-015-markdown) why. 
  
  - Some of our favorite open source desktop markdown editors
    
    - [Marktext](https://marktext.app/)
  
  - Some of our favorite open source web-based markdown editors
    
    - [StackEdit](https://stackedit.io/app#)

---
### Email

- [Godaddy - Workspace Email  Help](https://au.godaddy.com/help/server-and-port-settings-for-workspace-email-6949)
- Video: [How to Set up your GoDaddy Office 365 Email in Gmail (Full Tutorial)](https://www.youtube.com/watch?v=tHH0vVObeMw)


---
  
- Incoming settings 
	- Server name:  `outlook.office365.com`
	- Port:
		- if POP: `995` with SSL selected  
		- if IMAP: `993` with SSL selected  
	- Encryption method:  `TLS` (POP/IMAP)  

---
  
- Outgoing settings  
  - Server name:  `smtp.office365.com`
  - Port:`587`
  
- Encryption method:  `STARTTLS` or `TLS`

---

### Random

- connecting stackedit to gitlab: https://dev.to/maxwhite20062003/how-to-link-your-gitlab-account-to-stackedit-2pkc

### Random Errors

- warning: LF will be replaced by CRLF.
  
  - Solution
    1. `git config --system --unset core.autocrlf`
    2. `git config --global core.autocrlf true`

- clean filter git crypt failed
  
      - https://github.com/AGWA/git-crypt/issues/184#issuecomment-541942913
  
  C:\Users\ryan\.gnupg

- Git bash Error: Could not fork child process:
  
  - https://stackoverflow.com/questions/45799650/git-bash-error-could-not-fork-child-process-there-are-no-available-terminals/
  - kill the 'agent' process that you last used.
    - might be: gpg-agent.exe

- To reset harder!
  
  - https://stackoverflow.com/a/4327720

- Buffer error
  
  - RPC failed; HTTP 524 curl 22 The requested URL returned error: 524 fatal: the remote end hung up unexpectedly: https://confluence.atlassian.com/bitbucketserverkb/git-push-fails-fatal-the-remote-end-hung-up-unexpectedly-779171796.html
    - `git config --global http.postBuffer 157286400`

- [rm -f .git/index.lock](https://stackoverflow.com/questions/9282632/git-index-lock-file-exists-when-i-try-to-commit-but-cannot-delete-the-file/11466435#11466435)
  
  - [video](https://www.dropbox.com/s/msj8heiq7ryefy1/2022-02-03_10-48-13.mp4?dl=0)
  - Might solve following errors
    - `git did not exit cleanly (exit code 128)`
      - This error might be because there's a file with path that is too long

- assume-unchanged and skip-worktree flags
  
  - https://fallengamer.livejournal.com/93321.html

- Record audio and mic on ShareX: https://softwarerecs.stackexchange.com/questions/42767/screen-recording-tool-that-records-both-speaker-o-p-as-well-as-mic-input/74728#74728
  
  - ```-f dshow -i audio="virtual-audio-capturer" -filter_complex amix=inputs=2:duration=longest```

- Keycast: https://github.com/bfritscher/carnac/releases

- If Gitlab password is not working: https://stackoverflow.com/questions/47860772/gitlab-remote-http-basic-access-denied-and-fatal-authentication/51133684#51133684

%t%pn%y-%mo-%d_%h-%mi-%s_%pn




<!--

Username : Email address  
Password : Email password  
  
Incoming settings  
Server name:  
outlook.office365.com (POP/IMAP)  
  
Port:  
POP: 995 with SSL selected  
IMAP: 993 with SSL selected  
  
Encryption method:  
TLS (POP/IMAP)  
  
Outgoing settings  
Server name:  
smtp.office365.com  
  
Port:  
587  
  
Encryption method:  
STARTTLS

-->

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE2NDMzOTYzMzcsLTY5OTQwNjAxOSwxMj
QxODMxNjUxLDE1ODgxOTc2MTYsMTIyNDQ1NzkxNSwtOTIxMTE5
MzEwLDIxMjEzNDA5MjEsLTc5NzUwODU2MSw3MTY3MTQ0MCwtOD
Y4ODc4MjIsLTE0NzMzODU2OSwxMDM5ODkzODEwLDE0NDc0Mzgw
NTcsMTYzNjU3MzMyMSwtMTY4NTY4MTU1MiwtMTI2Nzc3NDg5NC
w2ODQzMDg4ODYsLTkxNzQxNzc3NSw3MDYyNjM5NTEsLTE1Mjk3
NTYyNDFdfQ==
-->